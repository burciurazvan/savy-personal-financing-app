SAVY - Personal Financing App

Savy is a unique and personal financing app meant to help you manage your incomes, expenses, loans and transfers easily, across multiple accounts or credit cards.

The app allows the user to:
- create an account using an email and a password
- add his credit card(s) or bank account(s) balance
- add income sources, expenses, loans or transfers from and between existing accounts
- see an overview of his activity (monthly/weekly statistics and charts)

Main screens of the app:

Login/Signup
Manage profile:
	- change personal details
	- add profile photo
	- add a password for security when opening the app
	- manage accounts
Dashboard:
	- see current balance and list of transactions (income/expense/transfer/loan)
	- add transaction (income/expense/transfer/loan)
		- input for adding a value and currency
		- add a description for the transaction
		- choose account that the transaction applies to
	- see weekly/monthly stats (charts)

